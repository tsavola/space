# Copyright 2006-2007  Timo Savola
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

-include BuildOptions.mk
include $(BUILD)/build.mk
include $(if $(COMMON),$(COMMON)/module.mk)

export BOOST_TEST_REPORT_LEVEL=no

test_NAME	= test
test_SOURCES	= $(wildcard test/*.cpp)
test_LIBS	+= -lboost_unit_test_framework

$(call test,test)

TAGS_SOURCES	= $(wildcard space/*.hpp)

install:
	install -d $(DESTDIR)$(PREFIX)/include/space
	install -m 644 space/*.[ht]pp $(DESTDIR)$(PREFIX)/include/space/

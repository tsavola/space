/*
 * Copyright 2004-2007  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

/**
 * \file space/angle.tpp
 * Angle computations - template implementation
 */

namespace space {

// Normalized angle

template <typename Unit, typename T>
angle<Unit,T>::angle() :
	Value(T(0))
{
}

template <typename Unit, typename T>
angle<Unit,T>::angle(T x) :
	Value(Unit::mod(x))
{
}

template <typename Unit, typename T>
template <typename Type>
angle<Unit,T>::angle(angle<Unit,Type> a) :
	Value(T(a.Value))
{
}

template <typename Unit, typename T>
angle<Unit,T> & angle<Unit,T>::operator=(T x)
{
	Value = Unit::mod(x);
	return *this;
}

template <typename Unit, typename T>
template <typename Type>
angle<Unit,T> & angle<Unit,T>::operator=(angle<Unit,Type> a)
{
	Value = T(a.Value);
	return *this;
}

template <typename Unit, typename T>
void angle<Unit,T>::operator+=(angle a)
{
	Value = Unit::mod(Value + a.Value);
}

template <typename Unit, typename T>
void angle<Unit,T>::operator-=(angle a)
{
	Value = Unit::mod(Value - a.Value);
}

template <typename Unit, typename T>
void angle<Unit,T>::operator+=(T x)
{
	Value = Unit::mod(Value + x);
}

template <typename Unit, typename T>
void angle<Unit,T>::operator-=(T x)
{
	Value = Unit::mod(Value - x);
}

template <typename Unit, typename T>
void angle<Unit,T>::operator*=(T x)
{
	Value = Unit::mod(Value * x);
}

template <typename Unit, typename T>
void angle<Unit,T>::operator/=(T x)
{
	Value = Unit::mod(Value / x);
}

template <typename Unit, typename T>
angle<Unit,T> angle<Unit,T>::operator+(angle a)
{
	return angle<Unit,T>(Value + a.Value);
}

template <typename Unit, typename T>
angle<Unit,T> angle<Unit,T>::operator-(angle a)
{
	return angle<Unit,T>(Value - a.Value);
}

template <typename Unit, typename T>
angle<Unit,T> angle<Unit,T>::operator+(T x)
{
	return angle<Unit,T>(Value + x);
}

template <typename Unit, typename T>
angle<Unit,T> angle<Unit,T>::operator-(T x)
{
	return angle<Unit,T>(Value - x);
}

template <typename Unit, typename T>
angle<Unit,T> angle<Unit,T>::operator*(T x)
{
	return angle<Unit,T>(Value * x);
}

template <typename Unit, typename T>
angle<Unit,T> angle<Unit,T>::operator/(T x)
{
	return angle<Unit,T>(Value / x);
}

template <typename Unit, typename T>
T angle<Unit,T>::radians() const
{
	return Unit::radians(Value);
}

template <typename Unit, typename T>
T angle<Unit,T>::degrees() const
{
	return Unit::degrees(Value);
}

// Radian system

template <typename T>
T radian::mod(T x)
{
	return std::fmod(x, T(M_PI * 2));
}

template <typename T>
T radian::radians(T x)
{
	return x;
}

template <typename T>
T radian::degrees(T x)
{
	return x * T(180 / M_PI);
}

// Degree system

template <typename T>
T degree::mod(T x)
{
	return std::fmod(x, T(360));
}

template <typename T>
T degree::radians(T x)
{
	return x * T(M_PI / 180);
}

template <typename T>
T degree::degrees(T x)
{
	return x;
}

// Rotation angle

template <typename T>
template <typename Unit, typename Type>
rot_angle<T>::rot_angle(angle<Unit,Type> a) :
	Sin(T(sin(a))),
	Cos(T(cos(a)))
{
}

template <typename T>
template <typename Type>
rot_angle<T>::rot_angle(const rot_angle<Type> & a) :
	Sin(T(a.Sin)),
	Cos(T(a.Cos))
{
}

template <typename T>
template <typename Unit, typename Type>
rot_angle<T> & rot_angle<T>::operator=(angle<Unit,Type> a)
{
	Sin = T(sin(a));
	Cos = T(cos(a));
	return *this;
}

template <typename T>
template <typename Type>
rot_angle<T> & rot_angle<T>::operator=(const rot_angle<Type> & a)
{
	Sin = T(a.Sin);
	Cos = T(a.Cos);
	return *this;
}

// Rotation axis

template <typename T>
template <typename Type>
rot_axis<T>::rot_axis(const vec<3,Type> & a)
{
	operator=(a);
}

template <typename T>
template <typename Type>
rot_axis<T>::rot_axis(const rot_axis<Type> & a) :
	U (T(a.U)),
	Un(T(a.Un)),
	S (T(a.S))
{
}

template <typename T>
template <typename Type>
rot_axis<T> & rot_axis<T>::operator=(const vec<3,Type> & a)
{
	U  = a * transpose(a);
	Un = mat<3,T>(1) - U;
	S  = mat<3,T>(   0, -a.z,  a.y,
			 a.z,    0, -a.x,
			 -a.y,  a.x,    0);
	return *this;
}

template <typename T>
template <typename Type>
rot_axis<T> & rot_axis<T>::operator=(const rot_axis<Type> & a)
{
	U  = T(a.U);
	Un = T(a.Un);
	S  = T(a.S);
	return *this;
}

// Operations

template <typename Unit, typename T>
T sin(angle<Unit,T> a)
{
	return sin(a.radians());
}

template <typename Unit, typename T>
T cos(angle<Unit,T> a)
{
	return cos(a.radians());
}

template <typename Unit, typename T>
T tan(angle<Unit,T> a)
{
	return tan(a.radians());
}

template <typename T>
T sin(const rot_angle<T> & a)
{
	return a.Sin;
}

template <typename T>
T cos(const rot_angle<T> & a)
{
	return a.Cos;
}

template <typename Angle, typename T>
mat<3,T> rotation(const Angle & a, const vec<3,T> & axis)
{
	return rotation(a, rot_axis<T>(axis));
}

template <typename Angle, typename T>
mat<3,T> rotation(const Angle & a, const rot_axis<T> & axis)
{
	return rotation(sin(a), cos(a), axis);
}

template <typename T>
mat<3,T> rotation(T sin, T cos, const rot_axis<T> & axis)
{
	return axis.U + axis.Un * cos + axis.S * sin;
}

} // space

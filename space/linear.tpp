/*
 * Copyright 2004-2007  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

/**
 * \file space/linear.tpp
 * Linear algebra - template implementation
 */

#include <boost/assert.hpp>
#include <boost/static_assert.hpp>
#include <algorithm>
#include <cmath>
#include <cstdlib>

namespace space {

// Column vector

template <vec_size N, typename T>
inline vec<N,T>::vec()
{
}

template <vec_size N, typename T>
inline vec<N,T>::vec(enum zero dummy)
{
	operator=(dummy);
}

template <vec_size N, typename T>
inline vec<N,T>::vec(T x, T y)
{
	BOOST_STATIC_ASSERT(N == 2);
	operator[](0) = x;
	operator[](1) = y;
}

template <vec_size N, typename T>
inline vec<N,T>::vec(T x, T y, T z)
{
	BOOST_STATIC_ASSERT(N == 3);
	operator[](0) = x;
	operator[](1) = y;
	operator[](2) = z;
}

template <vec_size N, typename T>
inline vec<N,T>::vec(T x, T y, T z, T w)
{
	BOOST_STATIC_ASSERT(N == 4);
	operator[](0) = x;
	operator[](1) = y;
	operator[](2) = z;
	operator[](3) = w;
}

template <vec_size N, typename T>
template <typename Type>
inline vec<N,T>::vec(const vec<N,Type> & v)
{
	operator=(v);
}

template <vec_size N, typename T>
template <typename Type>
vec<N,T>::vec(const vec<N-1,Type> & v, T s)
{
	for (vec_size i = 0; i < N - 1; i++)
		operator[](i) = v[i];

	operator[](N - 1) = s;
}

template <vec_size N, typename T>
template <typename Type>
vec<N,T>::vec(const vec<N-2,Type> & v, T s, T t)
{
	for (vec_size i = 0; i < N - 2; i++)
		operator[](i) = v[i];

	operator[](N - 2) = s;
	operator[](N - 1) = t;
}

template <vec_size N, typename T>
inline vec<N,T> & vec<N,T>::operator=(enum zero)
{
	for (vec_size i = 0; i < N; i++)
		operator[](i) = T(0);

	return *this;
}

template <vec_size N, typename T>
template <typename Type>
vec<N,T> & vec<N,T>::operator=(const vec<N,Type> & v)
{
	for (vec_size i = 0; i < N; i++)
		operator[](i) = v[i];

	return *this;
}

template <vec_size N, typename T>
inline T vec<N,T>::operator[](index_type i) const
{
	return reinterpret_cast<const T *> (this)[i];
}

template <vec_size N, typename T>
inline T & vec<N,T>::operator[](index_type i)
{
	return reinterpret_cast<T *> (this)[i];
}

template <vec_size N, typename T>
inline const T * vec<N,T>::data() const
{
	return reinterpret_cast<const T *> (this);
}

template <vec_size N, typename T>
inline T * vec<N,T>::data()
{
	return reinterpret_cast<T *> (this);
}

template <vec_size N, typename T>
template <vec_size Num>
inline const vec<Num,T> & vec<N,T>::sub() const
{
	BOOST_STATIC_ASSERT(Num <= N);
	return reinterpret_cast<const vec<Num,T> &> (*this);
}

template <vec_size N, typename T>
template <vec_size Num>
inline vec<Num,T> & vec<N,T>::sub()
{
	BOOST_STATIC_ASSERT(Num <= N);
	return reinterpret_cast<vec<Num,T> &> (*this);
}

template <vec_size N, typename T>
vec<N,T>::operator bool() const
{
	for (vec_size i = 0; i < N; i++)
		if (operator[](i))
			return true;
	return false;
}

template <vec_size N, typename T>
void vec<N,T>::normalize()
{
	*this /= length(*this);
}

// Row vector

template <vec_size N, typename T>
inline row_vec<N,T>::row_vec(const vec_type & v) :
	Vector(v)
{
}

template <vec_size N, typename T>
inline T row_vec<N,T>::operator[](index_type i) const
{
	return Vector[i];
}

// Vector operations

template <vec_size N, typename T>
bool operator==(const vec<N,T> & v, const vec<N,T> & u)
{
	for (vec_size i = 0; i < N; i++)
		if (v[i] != u[i])
			return false;
	return true;
}

template <vec_size N, typename T>
bool operator!=(const vec<N,T> & v, const vec<N,T> & u)
{
	for (vec_size i = 0; i < N; i++)
		if (v[i] == u[i])
			return false;
	return true;
}

template <vec_size N, typename T>
T length(const vec<N,T> & v)
{
	return std::sqrt(length2(v));
}

template <vec_size N, typename T>
T length2(const vec<N,T> & v)
{
	T p = 0;
	for (vec_size i = 0; i < N; i++)
		p += v[i] * v[i];
	return p;
}

template <vec_size N, typename T>
vec<N,T> normalize(const vec<N,T> & v)
{
	return v / length(v);
}

template <vec_size N, typename T>
inline row_vec<N,T> transpose(const vec<N,T> & v)
{
	return row_vec<N,T>(v);
}

template <vec_size N, typename T>
void swap(vec<N,T> & v, vec<N,T> & u)
{
	for (vec_size i = 0; i < N; i++)
		std::swap(v[i], u[i]);
}

// Square matrix

template <vec_size N, typename T>
inline mat<N,T>::mat()
{
}

template <vec_size N, typename T>
inline mat<N,T>::mat(enum identity dummy)
{
	operator=(dummy);
}

template <vec_size N, typename T>
inline mat<N,T>::mat(T ii, T ij,
		     T ji, T jj)
{
	BOOST_STATIC_ASSERT(N == 2);

	operator()(0,0) = ii; operator()(0,1) = ij;
	operator()(1,0) = ji; operator()(1,1) = jj;
}

template <vec_size N, typename T>
inline mat<N,T>::mat(T ii, T ij, T ik,
		     T ji, T jj, T jk,
		     T ki, T kj, T kk)
{
	BOOST_STATIC_ASSERT(N == 3);

	operator()(0,0) = ii; operator()(0,1) = ij; operator()(0,2) = ik;
	operator()(1,0) = ji; operator()(1,1) = jj; operator()(1,2) = jk;
	operator()(2,0) = ki; operator()(2,1) = kj; operator()(2,2) = kk;
}

template <vec_size N, typename T>
inline mat<N,T>::mat(T ii, T ij, T ik, T il,
                     T ji, T jj, T jk, T jl,
                     T ki, T kj, T kk, T kl,
                     T li, T lj, T lk, T ll)
{
	BOOST_STATIC_ASSERT(N == 4);

	operator()(0,0) = ii; operator()(0,1) = ij;
	operator()(0,2) = ik; operator()(0,3) = il;

	operator()(1,0) = ji; operator()(1,1) = jj;
	operator()(1,2) = jk; operator()(1,3) = jl;

	operator()(2,0) = ki; operator()(2,1) = kj;
	operator()(2,2) = kk; operator()(2,3) = kl;

	operator()(3,0) = li; operator()(3,1) = lj;
	operator()(3,2) = lk; operator()(3,3) = ll;
}

template <vec_size N, typename T>
template <vec_size Num, typename Type>
mat<N,T>::mat(const mat<Num,Type> & m)
{
	vec_size i, j;
	for (i = 0; i < std::min(N, Num); i++) {
		for (j = 0; j < std::min(N, Num); j++)
			operator()(i,j) = m(i,j);
		for (; j < N; j++)
			operator()(i,j) = T(0);
	}
	for (; i < N; i++)
		for (j = 0; j < N; j++)
			operator()(i,j) = T(i == j);
}

template <vec_size N, typename T>
mat<N,T> & mat<N,T>::operator=(enum identity)
{
	for (vec_size i = 0; i < N; i++)
		for (vec_size j = 0; j < N; j++)
			operator()(i,j) = T(i == j);

	return *this;
}

template <vec_size N, typename T>
template <typename Type>
mat<N,T> & mat<N,T>::operator=(const mat<N,Type> & m)
{
	for (vec_size i = 0; i < N; i++)
		for (vec_size j = 0; j < N; j++)
			operator()(i,j) = m(i,j);

	return *this;
}

template <vec_size N, typename T>
inline T mat<N,T>::operator()(index_type column, index_type row) const
{
	return Data[column * N + row];
}

template <vec_size N, typename T>
inline T & mat<N,T>::operator()(index_type column, index_type row)
{
	return Data[column * N + row];
}

template <vec_size N, typename T>
inline const mat_column<N,T> mat<N,T>::operator[](index_type column) const
{
	return mat_column<N,T>(*this, column);
}

template <vec_size N, typename T>
inline mat_column<N,T> mat<N,T>::operator[](index_type column)
{
	return mat_column<N,T>(*this, column);
}

template <vec_size N, typename T>
inline const T * mat<N,T>::data() const
{
	return reinterpret_cast<const T *> (this);
}

template <vec_size N, typename T>
void mat<N,T>::normalize()
{
	T::not_implemented_yet();
}

template <vec_size N, typename T>
void mat<N,T>::transpose()
{
	for (vec_size j = 0; j < N; j++)
		for (vec_size i = j; i < N; i++)
			if (i != j)
				std::swap(operator()(i,j), operator()(j,i));
}

// Matrix column vector

template <vec_size N, typename T>
inline mat_column<N,T>::mat_column(const mat<N,T> & m, index_type i) :
	Matrix(m),
	Column(i)
{
}

template <vec_size N, typename T>
inline mat_column<N,T> & mat_column<N,T>::operator=(const mat_column<N,T> & v)
{
	for (vec_size i = 0; i < N; i++)
		operator[](i) = v[i];
	return *this;
}

template <vec_size N, typename T>
inline mat_column<N,T> & mat_column<N,T>::operator=(const vec<N,T> & v)
{
	for (vec_size i = 0; i < N; i++)
		operator[](i) = v[i];
	return *this;
}

template <vec_size N, typename T>
inline T mat_column<N,T>::operator[](index_type i) const
{
	return Matrix(Column, i);
}

template <vec_size N, typename T>
inline T & mat_column<N,T>::operator[](index_type i)
{
	return const_cast <mat<N,T> &> (Matrix)(Column, i);
}

template <vec_size N, typename T>
inline mat_column<N,T>::operator vec<N,T>() const
{
	vec<N,T> r;
	for (vec_size i = 0; i < N; i++)
		r[i] = operator[](i);
	return r;
}

template <vec_size N, typename T>
inline mat_column<N,T>::operator bool() const
{
	for (vec_size i = 0; i < N; i++)
		if (operator[](i))
			return true;
	return false;
}

// Matrix operations

template <vec_size N, typename T>
mat<N,T> normalize(const mat<N,T> & v)
{
	return T::not_implemented_yet();
}

template <vec_size N, typename T>
mat<N,T> transpose(const mat<N,T> & m)
{
	mat<N,T> r;
	for (vec_size j = 0; j < N; j++)
		for (vec_size i = 0; i < N; i++)
			r[i][j] = m[j][i];
	return r;
}

template <vec_size N, typename T>
void swap(mat<N,T> & m, mat<N,T> & n)
{
	for (vec_size i = 0; i < N; i++)
		swap(m[i], n[i]);
}

// Operations

template <vec_size N, typename T>
inline const vec<N,T> & operator+(const vec<N,T> & v)
{
	return v;
}

template <vec_size N, typename T>
inline vec<N,T> operator-(const vec<N,T> & v)
{
	vec<N,T> r;
	for (vec_size i = 0; i < N; i++)
		r[i] = -v[i];
	return r;
}

template <vec_size N, typename T>
inline vec<N,T> operator+(const vec<N,T> & v, T x)
{
	vec<N,T> r;
	for (vec_size i = 0; i < N; i++)
		r[i] = v[i] + x;
	return r;
}

template <vec_size N, typename T>
inline vec<N,T> operator+(const vec<N,T> & v, const vec<N,T> & u)
{
	vec<N,T> r;
	for (vec_size i = 0; i < N; i++)
		r[i] = v[i] + u[i];
	return r;
}

template <vec_size N, typename T>
inline mat<N,T> operator+(const mat<N,T> & m, const mat<N,T> & n)
{
	mat<N,T> r;
	for (vec_size i = 0; i < N; i++) {
		const vec<N,T> v = m[i];
		const vec<N,T> u = n[i];
		r[i] = v + u;
	}
	return r;
}

template <vec_size N, typename T>
inline vec<N,T> operator*(const vec<N,T> & v, T x)
{
	vec<N,T> r;
	for (vec_size i = 0; i < N; i++)
		r[i] = v[i] * x;
	return r;
}

template <vec_size N, typename T>
inline mat<N,T> operator*(const vec<N,T> & v, row_vec<N,T> u)
{
	mat<N,T> r;
	for (vec_size i = 0; i < N; i++)
		r[i] = v * u[i];
	return r;
}

template <vec_size N, typename T>
inline mat<N,T> operator*(const mat<N,T> & m, T x)
{
	mat<N,T> r;
	for (vec_size i = 0; i < N; i++) {
		const vec<N,T> v = m[i];
		r[i] = v * x;
	}
	return r;
}

template <vec_size N, typename T>
inline vec<N,T> operator*(const mat<N,T> & m, const vec<N,T> & v)
{
	vec<N,T> r;
	for (vec_size i = 0; i < N; i++) {
		const vec<N,T> u = m[i];
		r[i] = dot(u, v);
	}
	return r;
}

template <vec_size N, typename T>
inline mat<N,T> operator*(const mat<N,T> & m, const mat<N,T> & n)
{
	mat<N,T> r;
	for (vec_size i = 0; i < N; i++)
		for (vec_size j = 0; j < N; j++) {
			T p = 0;
			for (vec_size k = 0; k < N; ++k)
				p += m[k][i] * n[j][k];
			r[j][i] = p;
		}
	return r;
}

template <vec_size N, typename T>
inline vec<N,T> operator-(const vec<N,T> & v, T x)
{
	return operator+(v, -x);
}

template <vec_size N, typename T>
inline vec<N,T> operator-(const vec<N,T> & v, const vec<N,T> & u)
{
	return operator+(v, -u);
}

template <vec_size N, typename T>
inline mat<N,T> operator-(const mat<N,T> & m, const mat<N,T> & n)
{
	mat<N,T> r;
	for (vec_size i = 0; i < N; i++) {
		const vec<N,T> v = m[i];
		const vec<N,T> u = n[i];
		r[i] = v + u;
	}
	return r;
}

template <vec_size N, typename T>
inline vec<N,T> operator/(const vec<N,T> & v, T x)
{
	return operator*(v, T(1) / x);
}

template <vec_size N, typename T>
inline mat<N,T> operator/(const mat<N,T> & m, T x)
{
	return operator*(m, T(1) / x);
}

template <vec_size N, typename T>
inline vec<N,T> & operator+=(vec<N,T> & v, T x)
{
	v = operator+(v, x);
	return v;
}

template <vec_size N, typename T>
inline vec<N,T> & operator+=(vec<N,T> & v, const vec<N,T> & u)
{
	v = operator+(v, u);
	return v;
}

template <vec_size N, typename T>
inline mat<N,T> & operator+=(mat<N,T> & m, const mat<N,T> & n)
{
	m = operator+(m, n);
	return m;
}

template <vec_size N, typename T>
inline vec<N,T> & operator*=(vec<N,T> & v, T x)
{
	v = operator*(v, x);
	return v;
}

template <vec_size N, typename T>
inline mat<N,T> & operator*=(mat<N,T> & m, T x)
{
	m = operator*(m, x);
	return m;
}

template <vec_size N, typename T>
inline mat<N,T> & operator*=(mat<N,T> & m, const mat<N,T> & n)
{
	m = operator*(m, n);
	return m;
}

template <vec_size N, typename T>
inline vec<N,T> & operator-=(vec<N,T> & v, T x)
{
	v = operator-(v, x);
	return v;
}

template <vec_size N, typename T>
inline vec<N,T> & operator-=(vec<N,T> & v, const vec<N,T> & u)
{
	v = operator-(v, u);
	return v;
}

template <vec_size N, typename T>
inline mat<N,T> & operator-=(mat<N,T> & m, const mat<N,T> & n)
{
	m = operator-(m, n);
	return m;
}

template <vec_size N, typename T>
inline vec<N,T> & operator/=(vec<N,T> & v, T x)
{
	v = operator/(v, x);
	return v;
}

template <vec_size N, typename T>
inline mat<N,T> & operator/=(mat<N,T> & m, T x)
{
	m = operator/(m, x);
	return m;
}

template <vec_size N, typename T>
inline T dot(const vec<N,T> & v, const vec<N,T> & u)
{
	T p = 0;
	for (vec_size i = 0; i < N; i++)
		p += v[i] * u[i];
	return p;
}

template <typename T>
inline vec<3,T> cross(const vec<3,T> & v, const vec<3,T> & u)
{
	return vec<3,T>(v.y * u.z - v.z * u.y,
	                v.z * u.x - v.x * u.z,
	                v.x * u.y - v.y * u.x);
}

} // space

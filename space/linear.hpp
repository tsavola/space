/*
 * Copyright 2004-2007  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

/**
 * \file space/linear.hpp
 * Linear algebra
 */

#ifndef SPACE_LINEAR_HPP
#define SPACE_LINEAR_HPP

#include <common/value.hpp>

namespace space {

enum zero { zero };
enum identity { identity };

typedef unsigned int vec_size;

template <vec_size N, typename T> struct mat_column;

/**
 * Vector component storage.
 */
template <vec_size N, typename T>
struct vec_data
{
private:
	T Data[N];
};

template <typename T>
struct vec_data<2,T>
{
	T x, y;
};

template <typename T>
struct vec_data<3,T>
{
	T x, y, z;
};

template <typename T>
struct vec_data<4,T>
{
	T x, y, z, w;
};

/**
 * Column vector.  Instances with \f$ 2 <= N <= 4 \f$ can access components
 * directly via \c x, \c y, \c z and \c w members (where applicable).  The
 * component data is stored sequentially.
 */
template <vec_size N, typename T = float>
struct vec : public vec_data<N,T>
{
	typedef T component_type;
	typedef common::value<0,N> index_type;

	vec();
	vec(enum zero);

	vec(T, T);
	vec(T, T, T);
	vec(T, T, T, T);

	template <typename Type> vec(const vec<N,  Type> &);
	template <typename Type> vec(const vec<N-1,Type> &, T);
	template <typename Type> vec(const vec<N-2,Type> &, T, T);

	vec & operator=(enum zero);

	template <typename Type> vec & operator=(const vec<N,Type> &);

	T   operator[](index_type) const;
	T & operator[](index_type);

	const T * data() const;
	T * data();

	template <vec_size Num> const vec<Num,T> & sub() const;
	template <vec_size Num>       vec<Num,T> & sub();

	operator bool() const;

	void normalize();
};

/**
 * Transposed column vector.
 */
template <vec_size N, typename T>
struct row_vec
{
	typedef vec<N,T> vec_type;
	typedef typename vec_type::component_type component_type;
	typedef typename vec_type::index_type index_type;

	explicit row_vec(const vec_type &);
	T operator[](index_type) const;

private:
	const vec_type & Vector;
};

// Vector operations

template <vec_size N, typename T> bool operator==(const vec<N,T> &,
                                                  const vec<N,T> &);

template <vec_size N, typename T> bool operator!=(const vec<N,T> &,
                                                  const vec<N,T> &);

template <vec_size N, typename T> T length(const vec<N,T> &);
template <vec_size N, typename T> T length2(const vec<N,T> &);
template <vec_size N, typename T> vec<N,T> normalize(const vec<N,T> &);
template <vec_size N, typename T> row_vec<N,T> transpose(const vec<N,T> &);

template <vec_size N, typename T> void swap(vec<N,T> &, vec<N,T> &);

/**
 * Square matrix.  Component access is possible in column-major order.  The
 * component data is stored in row-major order (OpenGL's native format).
 */
template <vec_size N, typename T = float>
struct mat
{
	typedef T component_type;
	typedef common::value<0,N> index_type;
	typedef mat_column<N,T> column_type;

	mat();
	mat(enum identity);

	mat(T, T,
	    T, T);

	mat(T, T, T,
	    T, T, T,
	    T, T, T);

	mat(T, T, T, T,
	    T, T, T, T,
	    T, T, T, T,
	    T, T, T, T);

	template <vec_size Num, typename Type>
	mat(const mat<Num,Type> &);

	mat & operator=(enum identity);

	template <typename Type>
	mat & operator=(const mat<N,Type> &);

	T   operator()(index_type, index_type) const;
	T & operator()(index_type, index_type);

	const column_type operator[](index_type) const;
	column_type operator[](index_type);

	const T * data() const;
	T * data();

	void normalize();
	void transpose();

private:
	mutable T Data[N * N];
};

/**
 * Column vector of a matrix.
 */
template <vec_size N, typename T>
struct mat_column
{
	typedef mat<N,T> mat_type;
	typedef typename mat_type::component_type component_type;
	typedef typename mat_type::index_type index_type;
	typedef vec<N,T> vec_type;

	mat_column(const mat_type &, index_type);

	mat_column & operator=(const mat_column &);
	mat_column & operator=(const vec_type &);

	T   operator[](index_type) const;
	T & operator[](index_type);

	operator vec_type() const;
	operator bool() const;

private:
	const mat_type & Matrix;
	const index_type Column;
};

// Matrix operations

template <vec_size N, typename T> mat<N,T> normalize(const mat<N,T> &);
template <vec_size N, typename T> mat<N,T> transpose(const mat<N,T> &);
template <vec_size N, typename T> void swap(mat<N,T> &, mat<N,T> &);

// Operations

template <vec_size N, typename T> const vec<N,T> & operator+(const vec<N,T> &);
template <vec_size N, typename T> vec<N,T> operator-(const vec<N,T> &);

template <vec_size N, typename T> vec<N,T> operator+(const vec<N,T> &, T);
template <vec_size N, typename T> vec<N,T> operator+(const vec<N,T> &,
                                                     const vec<N,T> &);
template <vec_size N, typename T> mat<N,T> operator+(const mat<N,T> &,
                                                     const mat<N,T> &);

template <vec_size N, typename T> vec<N,T> operator*(const vec<N,T> &, T);
template <vec_size N, typename T> mat<N,T> operator*(const vec<N,T> &,
                                                     row_vec<N,T>);
template <vec_size N, typename T> mat<N,T> operator*(const mat<N,T> &, T);
template <vec_size N, typename T> vec<N,T> operator*(const mat<N,T> &,
                                                     const vec<N,T> &);
template <vec_size N, typename T> mat<N,T> operator*(const mat<N,T> &,
                                                     const mat<N,T> &);

template <vec_size N, typename T> vec<N,T> operator-(const vec<N,T> &, T);
template <vec_size N, typename T> vec<N,T> operator-(const vec<N,T> &,
                                                     const vec<N,T> &);
template <vec_size N, typename T> mat<N,T> operator-(const mat<N,T> &,
                                                     const mat<N,T> &);

template <vec_size N, typename T> vec<N,T> operator/(const vec<N,T> &, T);
template <vec_size N, typename T> mat<N,T> operator/(const mat<N,T> &, T);

template <vec_size N, typename T> vec<N,T> & operator+=(vec<N,T> &, T);
template <vec_size N, typename T> vec<N,T> & operator+=(vec<N,T> &,
                                                        const vec<N,T> &);
template <vec_size N, typename T> mat<N,T> & operator+=(mat<N,T> &,
                                                        const mat<N,T> &);

template <vec_size N, typename T> vec<N,T> & operator*=(vec<N,T> &, T);
template <vec_size N, typename T> mat<N,T> & operator*=(mat<N,T> &, T);
template <vec_size N, typename T> mat<N,T> & operator*=(mat<N,T> &,
                                                        const mat<N,T> &);

template <vec_size N, typename T> vec<N,T> & operator-=(vec<N,T> &, T);
template <vec_size N, typename T> vec<N,T> & operator-=(vec<N,T> &,
                                                        const vec<N,T> &);
template <vec_size N, typename T> mat<N,T> & operator-=(mat<N,T> &,
                                                        const mat<N,T> &);

template <vec_size N, typename T> vec<N,T> & operator/=(vec<N,T> &, T);
template <vec_size N, typename T> mat<N,T> & operator/=(mat<N,T> &, T);

template <vec_size N, typename T> T dot(const vec<N,T> &, const vec<N,T> &);
template <typename T> vec<3,T> cross(const vec<3,T> &, const vec<3,T> &);

} // space

#include "linear.tpp"
#endif

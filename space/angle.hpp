/*
 * Copyright 2004-2007  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

/**
 * \file space/angle.hpp
 * Angle computations
 */

#ifndef SPACE_ANGLE_HPP
#define SPACE_ANGLE_HPP

#include "linear.hpp"
#include <cmath>

namespace space {

template <typename Unit, typename T> struct angle;
template <typename T> struct rot_angle;
template <typename T> struct rot_axis;

// Operations

using std::sin;
using std::cos;
using std::tan;

template <typename Unit, typename T> T sin(angle<Unit,T>);
template <typename Unit, typename T> T cos(angle<Unit,T>);
template <typename Unit, typename T> T tan(angle<Unit,T>);

template <typename T> T sin(const rot_angle<T> &);
template <typename T> T cos(const rot_angle<T> &);

template <typename Angle, typename T>
mat<3,T> rotation(const Angle &, const vec<3,T> &);

template <typename Angle, typename T>
mat<3,T> rotation(const Angle &, const rot_axis<T> &);

template <typename T>
mat<3,T> rotation(T sin, T cos, const rot_axis<T> &);

/**
 * Normalized angle.  A value-type that supports selected arithmetic
 * operations.  Its values will be wrapped to the range \f$ [0,max[ \f$, where
 * \f$ max \f$ is effectively defined by \c Unit::mod().  \param Unit the unit
 * system (space::radian or space::degree)
 */
template <typename Unit, typename T = float>
struct angle
{
	typedef T data_type;
	typedef Unit unit_type;

	angle();
	angle(T);

	template <typename Type> angle(angle<Unit,Type>);

	angle & operator=(T);

	template <typename Type> angle & operator=(angle<Unit,Type>);

	void operator+=(angle);
	void operator-=(angle);
	void operator+=(T);
	void operator-=(T);
	void operator*=(T);
	void operator/=(T);

	angle operator+(angle);
	angle operator-(angle);
	angle operator+(T);
	angle operator-(T);
	angle operator*(T);
	angle operator/(T);

	T radians() const;
	T degrees() const;

private:
	T Value;
};

/**
 * Defines the radian system for space::angle.
 */
struct radian
{
	template <typename T> static T mod(T);
	template <typename T> static T radians(T);
	template <typename T> static T degrees(T);
};

/**
 * Defines the degree system for space::angle.
 */
struct degree
{
	template <typename T> static T mod(T);
	template <typename T> static T radians(T);
	template <typename T> static T degrees(T);
};

/**
 * Constant rotation angle.  Stores precalculated values for rotation().
 */
template <typename T = float>
struct rot_angle
{
	friend T sin<T>(const rot_angle<T> &);
	friend T cos<T>(const rot_angle<T> &);

	template <typename Unit, typename Type>
	rot_angle(angle<Unit,Type>);

	template <typename Type>
	rot_angle(const rot_angle<Type> &);

	template <typename Unit, typename Type>
	rot_angle & operator=(angle<Unit,Type>);

	template <typename Type>
	rot_angle & operator=(const rot_angle<Type> &);

private:
	T Sin;
	T Cos;
};

/**
 * Constant rotation axis.  Stores precalculated values for rotation().
 */
template <typename T = float>
struct rot_axis
{
	friend mat<3,T> rotation<T>(T, T, const rot_axis<T> &);

	template <typename Type>
	rot_axis(const vec<3,Type> & normalized);

	template <typename Type>
	rot_axis(const rot_axis<Type> &);

	template <typename Type>
	rot_axis & operator=(const vec<3,Type> & normalized);

	template <typename Type>
	rot_axis & operator=(const rot_axis<Type> &);

private:
	mat<3,T> U;
	mat<3,T> Un;
	mat<3,T> S;
};

} // space

#include "angle.tpp"
#endif

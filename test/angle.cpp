/*
 * Copyright 2006-2007  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#include <space/angle.hpp>
#include <boost/test/unit_test_suite.hpp>

using namespace space;

namespace test {

void init_angle(boost::unit_test::test_suite *)
{
}

} // test

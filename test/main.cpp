/*
 * Copyright 2006-2007  Timo Savola
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

#include <boost/test/test_tools.hpp>
#include <boost/test/unit_test_suite.hpp>

using namespace boost::unit_test;

namespace test {

void init_angle(test_suite *);
void init_linear(test_suite *);

} // test

test_suite * init_unit_test_suite(int, char **)
{
	test_suite * suite = BOOST_TEST_SUITE();

	test::init_angle(suite);
	test::init_linear(suite);

	return suite;
}
